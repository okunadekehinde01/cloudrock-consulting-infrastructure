#DATA BASE CREATION

resource "aws_db_instance" "mysql" {
  allocated_storage    = 12
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mysql_db"
  username             = "kenny_db"
  password             = "goodclass"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
}

#RDS SUBNET GROUP

resource "aws_db_subnet_group" "subnet_group" {
  name       = "mysql"
  subnet_ids = [aws_subnet.priv_sub1.id, aws_subnet.priv_sub2.id]

  tags = {
    Name = "My_DB_subnet_group"
  }
}


# MySQL Security group
resource "aws_security_group" "mysql_secgrp" {
  name        = "mysql_secgrp"
  description = "Allow mysql inbound traffic"
  vpc_id      = aws_vpc.cloud_vpc.id

  ingress {
    description = "TLS from mysql"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}