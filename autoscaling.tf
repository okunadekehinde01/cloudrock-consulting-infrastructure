
resource "aws_autoscaling_group" "cloud_ASG" {
  name                      = "Autoscaling_group"
  max_size                  = 5
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 4
  force_delete              = true
  launch_configuration      = aws_launch_configuration.ASG_launch_config.name
  vpc_zone_identifier       = [aws_subnet.pub_sub1.id, aws_subnet.pub_sub2.id]
}


resource "aws_launch_configuration" "ASG_launch_config" {
  name                        = "ASG_launch"
  image_id                    = var.Instance_ami2
  instance_type               = var.Instance_type2
  key_name                    = "server_keypair"
  security_groups             = [aws_security_group.kenny_secgrp.id]
  associate_public_ip_address = true
}