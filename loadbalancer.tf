#CREATING APPLICATION LOAD BALANCER

resource "aws_lb" "cloud_alb" {
  name               = "Application-lb"
  internal           = true
  load_balancer_type = "application"
  security_groups    = [aws_security_group.kenny_secgrp.id]
  subnets            = [aws_subnet.pub_sub1.id, aws_subnet.pub_sub2.id]
}

#Target group
resource "aws_lb_target_group" "cloud_alb_tg" {
  name        = "cloud-alb-tg"
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.cloud_vpc.id

  health_check {
    protocol            = "HTTP"
    path                = "/"
    port                = 80
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 30
  }

}

#listeners

resource "aws_lb_listener" "ALB_listener" {
  load_balancer_arn = aws_lb.cloud_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.cloud_alb_tg.arn
  }
}