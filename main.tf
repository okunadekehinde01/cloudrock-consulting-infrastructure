# CREATING VPC

resource "aws_vpc" "cloud_vpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "cloud_vpc"
  }
}


#PUBLIC SUBNET
#Public subnet 1
resource "aws_subnet" "pub_sub1" {
  vpc_id            = aws_vpc.cloud_vpc.id
  cidr_block        = var.Pub1_cidr
  availability_zone = var.Az_2a

  tags = {
    Name = "pub_sub1"
  }
}

#public subnet 2
resource "aws_subnet" "pub_sub2" {
  vpc_id            = aws_vpc.cloud_vpc.id
  cidr_block        = var.Pub2_cidr
  availability_zone = var.Az_2b

  tags = {
    Name = "pub_sub2"
  }
}


#PPRIVATE SUBNET
#Private subnet 1
resource "aws_subnet" "priv_sub1" {
  vpc_id            = aws_vpc.cloud_vpc.id
  cidr_block        = var.Priv1_cidr
  availability_zone = var.Az_2a

  tags = {
    Name = "priv_sub1"
  }
}

#private subnet 2
resource "aws_subnet" "priv_sub2" {
  vpc_id            = aws_vpc.cloud_vpc.id
  cidr_block        = var.Priv2_cidr
  availability_zone = var.Az_2b

  tags = {
    Name = "priv_sub2"
  }
}



#ROUTE TABLES

#public route table
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.cloud_vpc.id

  tags = {
    Name = "public_route_table"
  }
}

#private route table
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.cloud_vpc.id

  tags = {
    Name = "private_route_table"
  }
}

#PUBLIC ROUTE TABLE ASSOCIATION

resource "aws_route_table_association" "public-route-association1" {
  subnet_id      = aws_subnet.pub_sub1.id
  route_table_id = aws_route_table.public_route_table.id
}


resource "aws_route_table_association" "public-route-association2" {
  subnet_id      = aws_subnet.pub_sub2.id
  route_table_id = aws_route_table.public_route_table.id
}


#PRIVATE ROUTE TABLE ASSOCIATION

resource "aws_route_table_association" "private-route-association1" {
  subnet_id      = aws_subnet.priv_sub1.id
  route_table_id = aws_route_table.private_route_table.id
}


resource "aws_route_table_association" "private-route-association2" {
  subnet_id      = aws_subnet.priv_sub2.id
  route_table_id = aws_route_table.private_route_table.id
}


#INTERNET GATEWAY

resource "aws_internet_gateway" "cloud_igw" {
  vpc_id = aws_vpc.cloud_vpc.id

  tags = {
    Name = "cloud_igw"
  }
}

# ROUTE ASSOCIATION

resource "aws_route" "public_route_assoc" {
  route_table_id         = aws_route_table.public_route_table.id
  destination_cidr_block = var.route_cidr
  gateway_id             = aws_internet_gateway.cloud_igw.id

}