#CREATING EC2 INSTANCE

resource "aws_instance" "cloud_ec2_1" {
  ami                         = var.Instance_ami1
  instance_type               = var.Instance_type1
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.kenny_secgrp.id]
  subnet_id                   = aws_subnet.pub_sub1.id
  availability_zone           = var.Az_2a
  key_name                    = "server_keypair"


  tags = {
    Name = "cloud_ec2_1"
  }
}



resource "aws_instance" "cloud_ec2_2" {
  ami                         = var.Instance_ami2
  instance_type               = var.Instance_type2
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.kenny_secgrp.id]
  subnet_id                   = aws_subnet.pub_sub2.id
  availability_zone           = var.Az_2b
  key_name                    = "server_keypair"


  tags = {
    Name = "cloud_ec2_2"
  }
}