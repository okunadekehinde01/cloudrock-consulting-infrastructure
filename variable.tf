#Region variable
variable "region_name" {
  default = "eu-west-2"
}

#Vpc cidr variable
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

#Public sub 1 cidr variable
variable "Pub1_cidr" {
  default = "10.0.1.0/24"
}

#Public sub 2 cidr variable
variable "Pub2_cidr" {
  default = "10.0.2.0/24"
}

#Private sub 1 cidr variable
variable "Priv1_cidr" {
  default = "10.0.3.0/24"
}

#Private sub 2 cidr variable
variable "Priv2_cidr" {
  default = "10.0.4.0/24"
}

#Availability zone 2a variable
variable "Az_2a" {
  default = "eu-west-2a"
}

#Availability zone 2b variable
variable "Az_2b" {
  default = "eu-west-2b"
}

#Instance type 1 variable
variable "Instance_type1" {
  default = "t2.micro"
}

#Instance type 2 variable
variable "Instance_type2" {
  default = "t2.micro"
}


#Instance ami 1 as variable
variable "Instance_ami1" {
  default = "ami-030770b178fa9d374"
}


#Instance ami 2 as variable
variable "Instance_ami2" {
  default = "ami-030770b178fa9d374"
}


# Route cidr variable
variable "route_cidr" {
  default = "0.0.0.0/0"
}
